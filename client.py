import socket
import argparse
import numpy as np
import matplotlib.pyplot as plt

TCP_IP = '192.168.7.2'
TCP_PORT = 5005
BUFFER_SIZE = 1024
MESSAGE = "Hello, World!"

parser = argparse.ArgumentParser(description='Process some flags.')
parser.add_argument('--code', dest='code', action='store',
                    default=1,)
parser.add_argument('--signal_type', dest='signal_type', action='store',
                    default=1,
                    help='nan')
parser.add_argument('--sampling_freq', dest='sampling_freq', action='store',
                    default=7,
                    help='nan')
parser.add_argument('--frequency', dest='frequency', action='store',
                    default=750,
                    help='nan')
parser.add_argument('--duty_cycle', dest='duty_cycle', action='store',
                    default=3,
                    help='nan')
parser.add_argument('--amplitude', dest='amplitude', action='store',
                    default=500,
                    help='nan')
parser.add_argument('--duration', dest='duration', action='store',
                    default=5,
                    help='nan')
parser.add_argument('--gain', dest='gain', action='store',
                    default=30,
                    help='nan')
parser.add_argument('--multiplier', dest='multiplier', action='store',
                    default=3,
                    help='nan')
parser.add_argument('--gain_ratio', dest='gain_ratio', action='store',
                    default=1,
                    help='nan')
parser.add_argument('--noise', dest='noise', action='store',
                    default=0,
                    help='nan')
parser.add_argument('--noise_level', dest='noise_level', action='store',
                    default=0,
                    help='nan')
parser.add_argument('--fft', dest='fft', action='store',
                    default=0,
                    help='nan')
args = parser.parse_args()
args_list = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
args_list[0] = args.code
args_list[1] = args.signal_type
args_list[2] = args.sampling_freq
args_list[3] = args.frequency
args_list[4] = args.duty_cycle
args_list[5] = args.amplitude
args_list[6] = args.duration
args_list[7] = args.gain
args_list[8] = args.multiplier
args_list[9] = args.gain_ratio
args_list[10] = args.noise
args_list[11] = args.noise_level
args_list[12] = args.fft

message = str.join('.', [ str(arg) for arg in args_list ])

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((TCP_IP, TCP_PORT))
s.send(str.encode(message))

data = s.recv(BUFFER_SIZE * 4)
s.close()

int32_t_data = [ int.from_bytes(data[i:i+4], byteorder='little') for i in range(0, len(data), 4) ]
int32_t_data_numpied = np.array(int32_t_data, dtype=np.int32)
print(int32_t_data)
plt.plot(int32_t_data_numpied)
plt.show()
